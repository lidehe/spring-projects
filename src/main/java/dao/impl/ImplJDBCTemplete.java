package dao.impl;


import bean.Admin;
import dao.AdminDao;
import org.springframework.jdbc.core.JdbcTemplate;

public class ImplJDBCTemplete implements AdminDao {
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private JdbcTemplate jdbcTemplate;

    public Admin printAllAdmin() {
        String sql = "select * from tb_admin;";
        jdbcTemplate.query(sql, resultSet -> {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("account");
            String pass = resultSet.getString("password");
            System.out.println(id + name + pass);
        });
        return null;
    }
}
