package dao.impl;

import bean.Admin;
import dao.AdminDao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

import static utils.JDBC.getConnection;
import static utils.JDBC.release;

public class ImplJDBC implements AdminDao {
    public Admin printAllAdmin(){
        Connection connection;
        try {
            connection = getConnection();
            String sql="select * from firs.tb_admin";
            Statement statement=connection.prepareStatement(sql);
            ResultSet rs=statement.executeQuery(sql);
            ResultSetMetaData rsmd= rs.getMetaData();
            System.out.println(rsmd.getColumnName(2));
            while (rs.next()){
                int id=rs.getInt("id");
                String name=rs.getString("account");
                String pass=rs.getString("password");
                System.out.println(id+name+pass);
            }
            release(rs,statement,connection);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
