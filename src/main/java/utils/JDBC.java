package utils;

import java.io.InputStream;
import java.sql.*;
import java.util.Properties;


public class JDBC {

    public static Connection getConnection() throws Exception {
        InputStream resourceAsStream = JDBC.class.getClassLoader().getResourceAsStream("db.properties");

        Properties properties = new Properties();
        properties.load(resourceAsStream);

        String url = properties.getProperty("jdbc.url");
        String userName = properties.getProperty("jdbc.username");
        String password = properties.getProperty("jdbc.password");
        String dirverClass = properties.getProperty("jdbc.driverClass");

        Class.forName(dirverClass);
        return DriverManager.getConnection(url, userName, password);
    }

    /**
     * 释放数据库连接的相关对象
     * @param resultSet 结果集
     * @param statement 语句
     * @param connection 连接
     */
    public static void release(ResultSet resultSet , Statement statement , Connection connection){
        if(resultSet != null){
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(statement != null){
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(connection != null){
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
