package springdata;

import org.springframework.data.repository.Repository;

import java.util.List;

public interface StaffRepository extends Repository<Staff,Integer> {
    Staff findByAccount(String account);

    List<Staff> findByAccountLike(String account);
}
