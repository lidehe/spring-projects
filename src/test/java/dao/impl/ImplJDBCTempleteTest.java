package dao.impl;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ImplJDBCTempleteTest {

    private ApplicationContext ac = null;
    private ImplJDBCTemplete jdbcTemplete=null;

    @Test
    public void printAllAdmin() {
        ac=new ClassPathXmlApplicationContext("springjdbctemplatecontext.xml");
        jdbcTemplete=(ImplJDBCTemplete)ac.getBean("adminDaoImplJDBCTemplete");
        jdbcTemplete.printAllAdmin();
    }
}