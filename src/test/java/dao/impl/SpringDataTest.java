package dao.impl;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import springdata.Staff;
import springdata.StaffRepository;

import java.util.List;
@Component
public class SpringDataTest {
    private ApplicationContext context = null;

    @Autowired
    private StaffRepository staffRepository = null;

    @Before
    public void getContext() {
        context = new ClassPathXmlApplicationContext("springdatacontext.xml");
        staffRepository = context.getBean(StaffRepository.class);
    }

    @Test
    public void testFind() {
        Staff staff = staffRepository.findByAccount("asdf");
        System.out.println(staff.toString());
    }

    @Test
    public void testFindLike() {
        List<Staff> staffs = staffRepository.findByAccountLike("%d%");
        staffs.forEach(a -> System.out.println(a.toString())
        );
    }
}
